# SvelteAPI와 FastAPI로 세련된 Hello World를 구축하기 단계별 가이드 <sup>[1](#footnote_1)</sup>

Svelte와 FastAPI로 재미있고 스타일리시한 웹 개발 여정을 시작할 준비가 되었나요? API 호출의 힘을 보여주는 간단한 "Hello World" 페이지를 만들어 보자!

<a name="footnote_1">1</a>: 이 페이지는 [Building Stylish Hello World with Svelte and FastAPI: A Step-by-Step Guide](https://medium.com/dev-genius/building-a-stylish-hello-world-with-svelte-and-fastapi-a-step-by-step-guide-bad5557d3794)를 편역한 것임.