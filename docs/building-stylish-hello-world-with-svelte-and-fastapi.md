
## Step 1: 백엔드(FastAPI) 설정
새 FastAPI 프로젝트 디렉토리를 만들고 해당 디렉토리로 이동한다.

```bash
$ mkdir hello-world-svelte && cd hello-world-svelte 
```

새로운 Python 가상 환경을 생성한다.

```bash
$ python -m venv venv
$ source venv/bin/activate
```

FastAPI를 설치한다.

```bash
$ pip install fastapi
$ pip install uvicorn
```

`main.py`라는 파일을 만들어 다음 코드를 추가하여 `hello()` API를 만든다.

```python
from fastapi import FastAPI 

app = FastAPI() 

@app.get("/hello") 
async def hello(): 
    return {"message": "Hello, world!"} 
```

FastAPI 서버를 실행한다.

```bash
$ uvicorn main:app --reload
```

FastAPI가 실행되면 FastAPI로 작성한 API는 URL http://127.0.0.1:8000/docs 을 호출하여 테스트해 볼 수 있다.

![](./images/fastapi_001.png)

## Step 2: 프런트엔드(Svelte) 설치와 웹 페이지 만들기

> **Note**: <br>
> [1-06 Svelte 개발환경 준비하기](https://wikidocs.net/175833)에서 "Svelte 설치하기" 참조

프로젝트 디렉토리에서 다음을 실행

```bash
$ npm create vite@latest frontend -- --template svelte
Need to install the following packages:
create-vite@5.0.0
Ok to proceed? (y) y

Scaffolding project in /Users/yoonjoonlee/MyProjects/PyCodingEx/SvelteFastAPI/frontend...

Done. Now run:

  cd frontend
  npm install
  npm run dev
```

그리고 다음과 같이 frontend 디렉터리로 이동한 후에 Svelte 어플리케이션을 설치한다.

```bash
$ cd frontend
$ npm install

added 38 packages, and audited 39 packages in 7s

3 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

다음 Svelte를 실행한다.

```bash
$ npm run dev

> frontend@0.0.0 dev
> vite

Forced re-optimization of dependencies

  VITE v5.0.6  ready in 740 ms

  ➜  Local:   http://localhost:5173/
  ➜  Network: use --host to expose
  ➜  press h + enter to show help

```

다음 브라우저에서 http://127.0.0.1:5173/을 방문하면, 다음과 같이 "Hello World"라는 문구가 화면 정중앙에 표시될 것이다.

![](./images/svelte_001.png)

화면 좌상단이 아닌 정중앙에 표시된 이유는 app.css 파일에 작성된 스타일 때문이다. 앞으로 이 곳에 정의된 스타일을 사용할 이유가 없으므로 `frontend/src/app.css` 파일의 내용도 모두 제거하자.

이제 Svelte에 작성한 `App.svelte` 파일에서 `hello` API를 호출하여 돌려받은 값을 화면에 출력해 보자. hello API는 호출하면 다음과 같은 json을 리턴한다.

```json
{"message": "Hello, world!"}
```

`App.svelte` 파일을 다음과 같이 바꾼다.

```js
<script>
  let message;

  fetch("http://127.0.0.1:8000/hello").then((response) => {
    response.json().then((json) => {
      message = json.message;
    });
  });
</script>

<h1>{message}</h1>
```

수정하고 브라우저에서 확인해 보자.

![](./images/svelteFastapi_001.png)

화면에는 undefined라고만 표시된다. message 변수에 값이 할당되지 않은 채로 그대로 남아있기 때문에 undefined 문자열이 출력된 것이다. 하지만 우리는 Hello API를 호출하여 돌려받은 값을 대입하였다. 그런데도 표시되지 않는 이유는 아마도 Hello API를 제대로 호출하지 못한 것 같다. 원인을 정확하게 파악하려면 브라우저 개발자 도구를 사용해야 한다. 크롬 브라우저에서 개발자 도구를 불러와 콘솔에 출력된 내용을 확인해 보자.

이유는 복잡하지만 간단히 말해 CORS 정책에 의해 요청이 거부되었다. 즉, 프론트엔드에서 FastAPI 백엔드 서버로 호출이 불가능한 상황이다. 이 오류는 FastAPI에 CORS 예외 URL을 등록하여 해결할 수 있다.

![](./images/svelteFastapi_002.png)

Svelte에서 fetch 함수는 비동기 함수이므로 다음과 같이 사용할 수도 있다.

```python
<script>
  async function hello() {
    const res = await fetch("http://127.0.0.1:8000/hello");
    const json = await res.json();

    if (res.ok) {
      return json.message;
    } else {
      alert("error");
    }
  }

  let promise = hello();
</script>

{#await promise}
  <p>...waiting</p>
{:then message}
  <h1>{message}</h1>
{/await}
```
